# Backend Assignment: Simple Todos and Reminder API

The assignment involves the creation of a ToDo and Reminder REST JSON API using ASP.NET Core. Please use the following libraries and versions:

* ASP.NET 6.0

## Simple ToDo API

Create a CRUD API for a simple ToDo management application. ToDos are organized in boards, on every board there can be multiple ToDos. A ToDo contains a title (str), done (bool), a created (datetime), updated (datetime) and due date(datetime) timestamp . A board has a name (str). Todos can be assigned to users with unique user id and email. 

Via a REST API it must be possible to:

*   List all boards
*   Add a new board
*   Change a board's title
*   Remove a board
*   List all ToDos on a board
*   List only uncompleted ToDos
*   Add a ToDo to a board
*   Change a ToDo's title or status
*   Delete a ToDo
*   Assign Todos to one or more users
*   If a ToDo is due, all users assigned to the todo should receive a notication via e-mail

User authentication is **NOT** required. The data doesn't need to be persisted, You can either use EF Core's in-memory database or a mock data repository that can hold data in a Dictionary/ConcurrentDictionary data structure. **However, please ensure that the mock data repository methods are asyncronous.**

This link gives an overview of adding test data and configuring in-memory database using EF Core: https://stormpath.com/blog/tutorial-entity-framework-core-in-memory-database-asp-net-core

Sending of the email can be mocked by simply printing the email id of the user in console instead of using a SMTP server to send an actual email.

## How to work on the assessment

*   Create a Gitlab account and let the recruiter know so that access can be granted to the repository (https://www.youtube.com/watch?v=Qz_RowKsU50)
*   Clone this repository
*   Create a new feature branch from main using the naming format fb/<your_name>_todo_assignment (https://www.tutorialspoint.com/gitlab/gitlab_create_branch.htm)
*   Create a Merge Request to the main branch (https://www.tutorialspoint.com/gitlab/gitlab_merge_requests.htm)
*   Checkout the feature branch in your local system  (git checkout fb/<your_name>_todo_assignment)
*   Start working on the assignment
*   Please do periodic commits with meaningful commit messages and push to the feature branch
*   Please include a brief description how to run your solution
*   If you have any questions contact the corresponding recruiter

Please note that we don't accept solutions without periodic commits or if we are unable to execute the solution.